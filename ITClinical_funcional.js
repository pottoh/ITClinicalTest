const assert = require('assert');

function testGetNthCharacters() {
  // Test N = 1
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 1), "ITCLNA");

  // Test N = 2
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 2), "TLN");

  // Test N = 3
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 3), "CNA");

  // Test N = 100
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 100), "");

  // Test N = -1
  assert.strictEqual(getNthCharacters("ITCLiNicAl", -1), "");
}

function getNthCharacters(s, n) {
  return s
    .split('')
    .filter((c, i) => (i+1) % n === 0 && c.toUpperCase() === c && n > 0)
    .join('');
}

testGetNthCharacters();