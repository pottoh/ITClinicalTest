const assert = require('assert');

function testGetNthCharacters() {
  // Test N = 1
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 1), "ITCLNA");

  // Test N = 2
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 2), "TLN");

  // Test N = 3
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 3), "CNA");

  // Test N = 100
  assert.strictEqual(getNthCharacters("ITCLiNicAl", 100), "");

  // Test N = -1
  assert.strictEqual(getNthCharacters("ITCLiNicAl", -1), "");
}

function getNthCharacters(s, n) {
  let result = "";
    for (let i = n-1; i >= 0 && i < s.length; i+=n) {
      const c = s.charAt(i);
      console.log(result);
      if (c.toUpperCase() === c) {
        result += c;
      }
    }
  console.log(result);
  return result;
}

testGetNthCharacters();

// Optional tasks:

// Test optional task 1
function opTaskOneTestGetNthCharacters() {
  // Test N = 1
  assert.strictEqual(opOneGetNthCharacters("!tCL1Nical", 2), "!CL1N");
}

function opOneGetNthCharacters(s, n) {
  let result = "";
  for (let i = 0; i < s.length; i++) {
    const c = s.charAt(i);
    if (c.toUpperCase() === c || !isNaN(c) || !c.toLowerCase() === c) {
      result += c;
    }
  }
  console.log(result);
  return result;
}

opTaskOneTestGetNthCharacters();

// Test optional task 2
function opTasktowTestGetNthCharacters() {
  // Test N = 1
  assert.strictEqual(opTwogetNthCharacters("ItCLINiCAL", 1), "ICLINCAL");
}

function opTwogetNthCharacters(s, n) {
  let result = "";
  const countMap = {};
  for (let i = n-1; i < s.length; i++) {
    const c = s.charAt(i);
    if (c.toUpperCase() === c || !isNaN(c) || !c.toLowerCase() === c) {
      result += c;
      countMap[c] = (countMap[c] || 0) + 1;
    }
  }
 console.log(countMap);
  return result;
}

opTasktowTestGetNthCharacters();